```bash=$
$yum install vim -y
$setenforce 0
$reboot
$sestatus

$useradd raf -m -s /bin/bash
$cat /etc/passwd
$groupadd admins
$visudo
%admins ALL=(ALL)    ALL
$usermod -aG admins raf
$groups raf

##SSH DEJA EFFECTUE DURANT LE TP##

$vi /etc/sysconfig/network-scripts/ifcfg-ens34
DNS1=1.1.1.1
DOMAIN=toto.lan

$lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   20G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0   19G  0 part
  ├─centos-root 253:0    0   17G  0 lvm  /
  └─centos-swap 253:1    0    2G  0 lvm  [SWAP]
sdb               8:16   0    3G  0 disk
sdc               8:32   0    2G  0 disk
sr0              11:0    1  918M  0 rom
$sudo pvcreate /dev/sdb ; pvcreate /dev/sdc
$ sudo pvs
  PV         VG     Fmt  Attr PSize   PFree
  /dev/sda2  centos lvm2 a--  <19.00g    0
  /dev/sdb          lvm2 ---    3.00g 3.00g
  /dev/sdc          lvm2 ---    2.00g 2.00g
  $ sudo vgcreate VG_data /dev/sdb
  Volume group "VG_data" successfully created 
$ sudo vgextend VG_data /dev/sdc
  Volume group "VG_data" successfully extended
$ sudo vgs
  VG      #PV #LV #SN Attr   VSize   VFree
  VG_data   2   0   0 wz--n-   4.99g 4.99g
$ sudo lvcreate -L 1G VG_data -n LV_data1
  Logical volume "LV_data1" created.
$ sudo lvcreate -L 1G VG_data -n LV_data2
  Logical volume "LV_data2" created.
$ sudo lvcreate -L 1G VG_data -n LV_data3
  Logical volume "LV_data3" created.
$ sudo lvs
  LV       VG      Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  LV_data1 VG_data -wi-a-----   1.00g
  LV_data2 VG_data -wi-a-----   1.00g
  LV_data3 VG_data -wi-a-----   1.00g
$ mkfs -t ext4 /dev/VG_data/LV_data1
mke2fs 1.42.9 (28-Dec-2013)
mkfs.ext4: Permission denied while trying to determine filesystem size
[toto@localhost dev]$ sudo kfs -t ext4 /dev/VG_data/LV_data1
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
65536 inodes, 262144 blocks
13107 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=268435456
8 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
##ON LE FAIT POUR LES 3##
$ sudo mkdir /mnt/part1
$ sudo mkdir /mnt/part2
$ sudo mkdir /mnt/part3
$ sudo mount /dev/VG_data/LV_data1 /mnt/part1/
$ sudo mount /dev/VG_data/LV_data2 /mnt/part2/
$ sudo mount /dev/VG_data/LV_data3 /mnt/part3/
$ $ df -h
Filesystem                    Size  Used Avail Use% Mounted on
devtmpfs                      475M     0  475M   0% /dev
tmpfs                         487M     0  487M   0% /dev/shm
tmpfs                         487M  7.7M  479M   2% /run
tmpfs                         487M     0  487M   0% /sys/fs/cgroup
/dev/mapper/centos-root        17G  1.5G   16G   9% /
/dev/sda1                    1014M  163M  852M  17% /boot
tmpfs                          98M     0   98M   0% /run/user/1000
/dev/mapper/VG_data-LV_data1  976M  2.6M  907M   1% /mnt/part1
/dev/mapper/VG_data-LV_data2  976M  2.6M  907M   1% /mnt/part2
/dev/mapper/VG_data-LV_data3  976M  2.6M  907M   1% /mnt/part3
$ sudo vi /etc/fstab 
/dev/mapper/VG_data/LV_data1 /mnt/part1         ext4    defaults        0 0
##POUR LES 3##
$vi /etc/fstab
/dev/VG_data/LV_data1 /mnt/part1                ext4    defaults        0 0
$ sudo umount /mnt/part1
$ sudo mount -av
$df -h 
$ df -h
Filesystem                    Size  Used Avail Use% Mounted on
devtmpfs                      475M     0  475M   0% /dev
tmpfs                         487M     0  487M   0% /dev/shm
tmpfs                         487M  7.7M  479M   2% /run
tmpfs                         487M     0  487M   0% /sys/fs/cgroup
/dev/mapper/centos-root        17G  1.5G   16G   9% /
/dev/sda1                    1014M  163M  852M  17% /boot
/dev/mapper/VG_data-LV_data1  976M  2.6M  907M   1% /mnt/part1
tmpfs                          98M     0   98M   0% /run/user/1000


$ systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
   Active: active (running) since Tue 2020-12-15 16:16:21 CET; 6min ago
     Docs: man:firewalld(1)
 Main PID: 775 (firewalld)
   CGroup: /system.slice/firewalld.service
           └─775 /usr/bin/python2 -Es /usr/sbin/firewalld --nofork --nopid

Dec 15 16:16:21 localhost.localdomain systemd[1]: Starting firewalld - dynamic firewall daemon...
Dec 15 16:16:21 localhost.localdomain systemd[1]: Started firewalld - dynamic firewall daemon.
Dec 15 16:16:21 localhost.localdomain firewalld[775]: WARNING: AllowZoneDrifting is enabled. This is considered an insecure configuration option. It will be removed in a future release. Please co...bling it now.
Hint: Some lines were ellipsized, use -l to show in full.
$ sudo vi /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python2 -m SimpleHTTPServer 8888

[Install]
WantedBy=multi-user.target
$ sudo systemctl daemon-reload
$ sudo systemctl start web
$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2020-12-15 16:35:07 CET; 4s ago
 Main PID: 1494 (python2)
   CGroup: /system.slice/web.service
           └─1494 /bin/python2 -m SimpleHTTPServer 8888

Dec 15 16:35:07 localhost.localdomain systemd[1]: Started Very simple web service.
$ sudo systemctl enable web
Created symlink from /etc/systemd/system/multi-user.target.wants/web.service to /etc/systemd/system/web.service.

##ARBO SUR UNE PAGE WEB VIA LE NAVIAGTEUR##

$ useradd web
$ passwd web
$ vi /etc/systemd/system/web.service
# cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python2 -m SimpleHTTPServer 8888
WorkingDirectory=/srv/www/
User=web

[Install]
WantedBy=multi-user.target

$ mkdir /srv/www/
$ cd /srv/www/
$ touch test 
$ systemctl deamon-reload
$ systemctl restart web

##ON A BIEN ACCES##



*FIN*

