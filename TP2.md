``````TP2
##A REALISER SUR LES 3 SRV##
##PREREQUIS, REGENERER ADRESSE MAC SUR LES 3 SRV##

$ sudo vi /etc/sysconfig/network-scripts/ifcfg-ens34
##CHANGER IP et DOMAINE##
$ ip a
##VERIFIER QUE L'IP STATIC A BIEN CHANGE##

$ sudo vi /etc/hosts
10.55.55.11 rp rp.tp2.cesi
10.55.55.12 db db.tp2.cesi
10.55.55.13 web web.tp2.cesi
$ sudo vi /etc/hostname
rp.tp2.cesi
$reboot
$ hostname
rp.tp2.cesi

$sudo systemctl start firewalld
$ systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; disabled; vendor preset: enabled)
   Active: active (running) since Wed 2020-12-16 10:57:14 CET; 5s ago
  
  
 - BDD - 
 
$ sudo yum install mariadb-server
$ rpm -qa | grep maria
mariadb-server-5.5.68-1.el7.x86_64
mariadb-libs-5.5.68-1.el7.x86_64
mariadb-5.5.68-1.el7.x86_64

$ sudo systemctl start mariadb
$ sudo systemctl enable mariadb
Created symlink from /etc/systemd/system/multi-user.target.wants/mariadb.service to /usr/lib/systemd/system/mariadb.service.
$ sudo systemctl status mariadb
● mariadb.service - MariaDB database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2020-12-16 11:03:10 CET; 37s ago
   
$ sudo ss -alnpt
State      Recv-Q Send-Q               Local Address:Port                              Peer Address:Port
LISTEN     0      50                               *:3306                                         *:*       

$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n]
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!

$ mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 13
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>CREATE DATABASE wordpress;
Query OK, 1 row affected (0.00 sec)
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| wordpress          |
+--------------------+
4 rows in set (0.00 sec)
MariaDB [(none)]> CREATE USER toto@localhost IDENTIFIED BY 'ton_password';
Query OK, 0 rows affected (0.00 sec)
MariaDB [(none)]> GRANT ALL PRIVILEGES ON wordpress.* TO toto@localhost IDENTIFIED BY 'toto';
MariaDB [(none)]> GRANT ALL PRIVILEGES ON wordpress.* TO toto@10.55.55.13 IDENTIFIED BY 'toto';  ##POUR ACCES DEPUIS SRV WEB AVEC USER TOTO##
Query OK, 0 rows affected (0.00 sec)
MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.01 sec)
MariaDB [(none)]> exit
Bye

##ON VA TESTER LA CONNEXION A LA DB WORDPRESS AVEC LE USER TOTO##
$ mysql -u toto -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 14
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| wordpress          |
+--------------------+
2 rows in set (0.00 sec)

MariaDB [(none)]>exit
Bye

$ sudo firewall-cmd --permanent --add-port=3306/tcp
success
$ sudo firewall-cmd --permanent --add-service=mysql
success



  - WEB - 
 
$ sudo yum install httpd -y
$ rpm -qa | grep httpd
httpd-2.4.6-97.el7.centos.x86_64
httpd-tools-2.4.6-97.el7.centos.x86_64

$ sudo systemctl start httpd
$ sudo systemctl enable httpd
Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.
$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2020-12-16 11:48:41 CET; 12s ago
 
$ yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
$ yum install yum-utils
$ yum-config-manager --enable remi-php72   
$yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo
 
$ sudo firewall-cmd --permanent --add-port=80/tcp
[sudo] password for toto:
success
$ sudo firewall-cmd --permanent --add-service=http
success
$ sudo systemctl restart firewalld

$ yum install wget -y
$ cd /tmp/
$ wget http://wordpress.org/latest.tar.gz
--2020-12-16 11:43:55--  http://wordpress.org/latest.tar.gz
Resolving wordpress.org (wordpress.org)... 198.143.164.252
Connecting to wordpress.org (wordpress.org)|198.143.164.252|:80... connected.
HTTP request sent, awaiting response... 301 Moved Permanently
Location: https://wordpress.org/latest.tar.gz [following]
--2020-12-16 11:43:56--  https://wordpress.org/latest.tar.gz
Connecting to wordpress.org (wordpress.org)|198.143.164.252|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 15422346 (15M) [application/octet-stream]
Saving to: ‘latest.tar.gz’

100%[==============================================================================>] 15,422,346  3.96MB/s   in 4.2s

2020-12-16 11:44:00 (3.49 MB/s) - ‘latest.tar.gz’ saved [15422346/15422346]

$ ll
total 15068
-rwx------. 1 root root      836 Dec 14 14:36 ks-script-vQMGJT
-rw-rw-r--. 1 toto toto 15422346 Dec  8 23:14 latest.tar.gz
-rw-------. 1 root root        0 Dec 14 14:32 yum.log

$ tar -xzvf latest.tar.gz
$ ll
total 15072
-rwx------. 1 root   root       836 Dec 14 14:36 ks-script-vQMGJT
-rw-rw-r--. 1 toto   toto  15422346 Dec  8 23:14 latest.tar.gz
drwx------. 3 root   root        17 Dec 16 11:48 systemd-private-9dfc0f47856b437bbfa2ef9024cdccfd-httpd.service-k6oJlE
drwxr-xr-x. 5 nobody 65534     4096 Dec  8 23:13 wordpress
-rw-------. 1 root   root         0 Dec 14 14:32 yum.log

$ cp -r /tmp/wordpress/* /var/www/html/
$ sudo chown -R apache:apache /var/www/html/*
$ ll /var/www/html
$ ll
total 212
-rw-r--r--.  1 apache apache   405 Dec 16 12:08 index.php
-rw-r--r--.  1 apache apache 19915 Dec 16 12:08 license.txt
-rw-r--r--.  1 apache apache  7278 Dec 16 12:08 readme.html
-rw-r--r--.  1 apache apache  7101 Dec 16 12:08 wp-activate.php
drwxr-xr-x.  9 apache apache  4096 Dec 16 12:08 wp-admin
-rw-r--r--.  1 apache apache   351 Dec 16 12:08 wp-blog-header.php
-rw-r--r--.  1 apache apache  2328 Dec 16 12:08 wp-comments-post.php
-rw-r--r--.  1 apache apache  2913 Dec 16 12:08 wp-config-sample.php
drwxr-xr-x.  4 apache apache    52 Dec 16 12:08 wp-content
-rw-r--r--.  1 apache apache  3939 Dec 16 12:08 wp-cron.php
drwxr-xr-x. 25 apache apache  8192 Dec 16 12:08 wp-includes
-rw-r--r--.  1 apache apache  2496 Dec 16 12:08 wp-links-opml.php
-rw-r--r--.  1 apache apache  3300 Dec 16 12:08 wp-load.php
-rw-r--r--.  1 apache apache 49831 Dec 16 12:08 wp-login.php
-rw-r--r--.  1 apache apache  8509 Dec 16 12:08 wp-mail.php
-rw-r--r--.  1 apache apache 20975 Dec 16 12:08 wp-settings.php
-rw-r--r--.  1 apache apache 31337 Dec 16 12:08 wp-signup.php
-rw-r--r--.  1 apache apache  4747 Dec 16 12:08 wp-trackback.php
-rw-r--r--.  1 apache apache  3236 Dec 16 12:08 xmlrpc.php

$ cp wp-config-sample.php wp-config.php
$ sudo vi wp-config.php 

## PLACER CES INFOS (user, bdd, host)

``````
![](https://i.imgur.com/RkfbFok.png)


````````Reverse Proxy ''''''

  - RP -

$ sudo yum install epel-release
$ sudo yum install nginx 
$ sudo systemctl start nginx
$ sudo systemctl enable nginx 
$ sudo cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bck
$ sudo echo "" > /etc/nginx/nginx.conf
$ sudo vi /etc/nginx/nginx.conf
events {}

http {
    server {
        listen       80;

        server_name web.cesi;

        location / {
            proxy_pass   http://10.55.55.13:80;
        }
    }
}

$ sudo systemctl restart nginx
$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2020-12-16 14:41:31 CET; 2s ago
  Process: 4597 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 4594 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 4592 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 4599 (nginx)
   CGroup: /system.slice/nginx.service
           ├─4599 nginx: master process /usr/sbin/nginx
           ├─4600 nginx: worker process
           └─4601 nginx: worker process
           

$ firewall-cmd --permanent --add-port=80/tcp
$ firewall-cmd --permanent --add-service=http
$ systemctl restart firewalld

C:\Windows\System32\drivers\etc\hosts
10.55.55.11 web.cesi

http://web.cesi 
##REDIRECTION OK##




 - Un peu de sécu - 
 
$ sudo yum install fail2ban
$ sudo systemctl start fail2ban
$ sudo systemctl enable fail2ban
Created symlink from /etc/systemd/system/multi-user.target.wants/fail2ban.service to /usr/lib/systemd/system/fail2ban.service.
$ sudo vi /etc/fail2ban/jail.local

#SUR UN SERVEUR VERS LE RP#
$ ssh toto@10.55.55.11
toto@10.55.55.11's password:
Permission denied, please try again.
toto@10.55.55.11's password:
Permission denied, please try again.
toto@10.55.55.11's password:
Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
$ ssh toto@10.55.55.11
ssh: connect to host 10.55.55.11 port 22: Connection refused

$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout web.cesi.key -out web.cesi.crt
$ sudo cp ~/web.cesi.crt /etc/pki/tls/certs/
$ ls -al /etc/pki/tls/certs/
total 16
drwxr-xr-x. 2 root root  137 Dec 17 10:18 .
drwxr-xr-x. 5 root root  104 Dec 16 14:13 ..
lrwxrwxrwx. 1 root root   49 Dec 14 15:48 ca-bundle.crt -> /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem
lrwxrwxrwx. 1 root root   55 Dec 14 15:48 ca-bundle.trust.crt -> /etc/pki/ca-trust/extracted/openssl/ca-bundle.trust.crt
-rwxr-xr-x. 1 root root  610 Aug  9  2019 make-dummy-cert
-rw-r--r--. 1 root root 2516 Aug  9  2019 Makefile
-rwxr-xr-x. 1 root root  829 Aug  9  2019 renew-dummy-cert
-rw-r--r--. 1 root root 1289 Dec 17 10:18 web.cesi.crt
$ sudo cp ~/web.cesi.key /etc/pki/tls/private/
$ ls -al /etc/pki/tls/private/
total 4
drwxr-xr-x. 2 root root   26 Dec 17 10:18 .
drwxr-xr-x. 5 root root  104 Dec 16 14:13 ..
-rw-r--r--. 1 root root 1704 Dec 17 10:18 web.cesi.key

$ sudo firewall-cmd --permanent --add-port=21/tcp
success
$ sudo firewall-cmd --permanent --add-service=ftp
succes 
$ sudo systemctl restart firewalld

$ sudo vi nginx.conf
http {
        server {

                listen       443;

                server_name web.cesi;
                ssl on;
                ssl_certificate "/etc/pki/tls/certs/web.cesi.crt";
                ssl_certificate_key "/etc/pki/tls/private/web.cesi.key";

        location / {
            proxy_pass   http://10.55.55.13:80;
        }
   }
        server {
                listen 80;
                server_name web.cesi;
                return 301 https://web.cesi;
        }
}

$ sudo systemctl restart nginx

## REDIRECTION EN HTTPS ET SSL OK ###

 
 
 - Monitoring -
 
 ### A REALISER SUR LES 3 SRV ###

$ bash <(curl -Ss https://my-netdata.io/kickstart.sh)
$ sudo firewall-cmd --add-port=19999/tcp --permanent
"success"
$ sudo systemctl restart firewalld

````````
![](https://i.imgur.com/uc67byA.png)

````
$ sudo vi health_alarm_notify.conf
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discordapp.com/api/webhooks/789110476306251846/rspW8iJbVv6_xzr0sdR1Bdl7qHvzROAaG_Hs4WmLE_bABOSQR_x9PxwqV4wof3PFe1QO"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"

$ sudo systemctl restart netdata 


 - BONUS -
 
$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout web.cesi.key -out web.cesi.crt

$ sudo vi /etc/nginx/nginx.conf
##AJOUTER LE BLOC SUIVANT POUR LA REDIRECTION SUR UN PORT DEJA USE##
##NE PAS OUBLIER DE MODIFIER LE FICHIER HOSTS##
server {
                listen 443;
                server_name n1.web.cesi;
                ssl on;
                ssl_certificate "/etc/pki/tls/certs/all.cesi.crt";
                ssl_certificate_key "/etc/pki/tls/private/all.cesi.key";

        location / {
                proxy_pass http://10.55.55.12:19999;
        }
   }
   
   

$ sudo systemctl restart nginx

``````
![](https://i.imgur.com/cmtrb72.png)

``````
